import { HeaderComponent } from './header.component';
import { SettingsComponent } from './../../elements/header/settings/settings.component';
import { SocialsComponent } from './../../elements/header/socials/socials.component';
import { NavComponent } from './../../elements/header/nav/nav.component';
import { LogoComponent } from './../../elements/header/logo/logo.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    HeaderComponent,
    LogoComponent,
    NavComponent,
    SocialsComponent,
    SettingsComponent,
  ],
  imports: [CommonModule],
  exports: [HeaderComponent],
})
export class HeaderModule {}

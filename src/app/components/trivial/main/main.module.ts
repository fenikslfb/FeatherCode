import { MainComponent } from './main.component';
import { OthersComponent } from './../../elements/main/others/others.component';
import { CoursesComponent } from './../../elements/main/courses/courses.component';
import { ProjectsComponent } from './../../elements/main/projects/projects.component';
import { AboutComponent } from './../../elements/main/about/about.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    MainComponent,
    AboutComponent,
    ProjectsComponent,
    CoursesComponent,
    OthersComponent,
  ],
  imports: [CommonModule],
  exports: [MainComponent],
})
export class MainModule {}
